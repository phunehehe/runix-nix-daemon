# [Nix Daemon](http://nixos.org/nix/manual/#sec-nix-daemon) for [Runix](https://gitlab.com/phunehehe/runix)

[![build status](https://gitlab.com/phunehehe/runix-nix-daemon/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-nix-daemon/commits/master)

This Runix app runs `nix-daemon` to provide [Nix Multi-User Mode](http://nixos.org/nix/manual/#ssec-multi-user).