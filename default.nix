{ pkgs, ... }: pkgs.writeBash (baseNameOf ./.) ''

  ${pkgs.glibc.bin}/bin/getent group nixbld || ${pkgs.shadow}/bin/groupadd --system nixbld

  for n in {1..10}
  do
    user="nixbld$n"
    ${pkgs.coreutils}/bin/id "$user" || ${pkgs.shadow}/bin/useradd \
      --comment "Nix build user $n" \
      --home /var/empty \
      --gid nixbld \
      --groups nixbld \
      --shell "$(which nologin)" \
      -M --no-user-group --system "$user"
  done

  exec ${pkgs.nix}/bin/nix-daemon
''
